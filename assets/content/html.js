export default [
	{
		"layout": "intro",
		"section": {
			"color": "white",
			"id": "intro"
		},
		"content": {
			"pre": [
				{
					"style": "normal",
					"text": "26 июня в 15:00 по Москве"
				},
				{
					"style": "bold",
					"text": "Бесплатно"
				}
			],
			"title": "Мастер-класс: <mark>создание сайтов</mark> без кода",
			"text": "Бесплатный мастер-класс. Мы познакомимся с профессией веб-дизайнера, поработаем в графическом редакторе, изучим основы теории и попрактикуемся в работе над сайтами. Освой азы одной из самых популярных профессий за один вечер!",
			"image": {
				"src": "/marathon/intro@2x.png",
				"width": 740
			},
			"button": {
				"text": "Записаться на мастер-класс",
				"emoji": "🤟🏻",
				"href": "#signup"
			}
		}
	},

	{
		"layout": "features",
		"section": {
			"color": "light",
			"title": "<mark>Программа</mark> мастер-класса",
			"text": "Мы постарались уложить всё самое важное для старта в веб-дизайне в формат интересной и насыщенной видео-лекции."
		},
		"content": {
			"columns": 3,
			"image_height": "100px",
			"cards": [
				{
					"emoji": "🖖🏻",
					"title": "Знакомство с веб-дизайном",
					"text": "Для начала мы&nbsp;познакомимся с&nbsp;профессией веб-дизайнера и&nbsp;её&nbsp;особенностями. Мы&nbsp;расскажем о&nbsp;перспективах в&nbsp;этой сфере и&nbsp;поделимся собственным опытом."
				},
				{
					"emoji": "🔨",
					"title": "Изучение инструментов",
					"text": "На мастер-класссе мы&nbsp;расскажем тебе о&nbsp;самых важных инструментах и&nbsp;на&nbsp;пальцах покажем, как ими пользоваться. И&nbsp;конечно&nbsp;же, ответим на&nbsp;вопросы."
				},
				{
					"emoji": "🖍",
					"title": "Практика в дизайне",
					"text": "Используя полученные знания, мы&nbsp;спроектируем и&nbsp;нарисуем небольшой сайт на&nbsp;произвольную тематику. Никакой магии&nbsp;&mdash; всё будет делаться у&nbsp;тебя на&nbsp;глазах."
				},
				{
					"emoji": "📎",
					"title": "40 000 домашних работ",
					"text": "Мы подарим тебе доступ к нашему крутому инструменту: он позволяет генерировать бесконечно много интересных задач для твоей будущей практики в дизайне."
				},
				{
					"emoji": "🎁",
					"title": "Подарки участникам",
					"text": "Абсолютно все участники мастер-класса получат приятные бонусы и ценные советы по дальнейшему развитию в профессии веб-дизайнера."
				}
			]
		}
	},

	{
		"layout": "features",
		"section": {
			"id": "for",
			"color": "light",
			"title": "<mark>Для кого</mark> этот мастер-класс?",
			"text": "Мы&nbsp;рады видеть на&nbsp;мастер-классе каждого. Наибольшую пользу получат те, кто пока что ничего не&nbsp;знает о&nbsp;веб-дизайне или делает свои первые шаги."
		},
		"content": {
			"columns": 2,
			"cards": [
				{
					"image": "/marathon/card-1.png",
					"title": "Для тех, кто в&nbsp;творчестве",
					"text": "Ты&nbsp;профессионально занимаешься архитектурой, интерьерами, живописью или другим видом искусства, и&nbsp;хочешь конвертировать свои навыки в&nbsp;стабильный заработок."
				},
				{
					"image": "/marathon/card-2.png",
					"title": "Для тех, кто ищет себя",
					"text": "Ты&nbsp;ищешь своё призвание или хочешь овладеть новым ремеслом, которое позволит работать удалённо и&nbsp;по-настоящему любить&nbsp;то, чем ты&nbsp;занимаешься."
				}
			]
		}
	},

	{
		"layout": "text-half",
		"section": {
			"id": "author",
			"color": "white",
			"title": "Валерий Алексеев",
			"text": "UX/UI Дизайнер, преподаватель и наставник"
		},
		"content": {
			"swap": false,
			"columns": 7,
			"align": "start",
			"media": {
				"type": "image",
				"image": "/valery-full.jpg"
			},
			features: [
				"Родился в провинциальном городе. Занимался дизайном с 15 лет. Первый сайт сделал в университете за 1000 рублей.",
				"<strong>Знаю весь процесс:</strong> от начала до конца. Сайт этого мастер-класса создан лично мной: дизайн, вёрстка, анимации, интеграция с CMS и CRM.",
				"Прошёл трёхмесячную стажировку в лучшей студии России — AIC – на должности Junior-дизайнера. На данный момент занимаю должность старшего UI/UX дизайнера в американском агентстве. Работаю полностью удалённо.",
				"Самостоятельно разработал, развил и продал онлайн-сервис для продвижения аккаунтов в Instagram. Развиваю платформы Coursly и Eduverse.",
				"Путешествую и работаю всю жизнь: Россия, Грузия, Армения, Израиль, Таиланд, Индия, Черногория — за последние два года."
			],
			"text": "<p>Привет! Я занимаюсь дизайном уже 10 лет. Первые 2-3 года я набивал шишки, совершал <strong>много ошибок</strong> и не понимал, как развиваться в веб-дизайне. А прямо сейчас я удалённо работаю дизайнером <strong>в компании из США</strong> и веду несколько крутых проектов.</p><p>Моя задача — помочь тебе сократить мой длинный путь в 10 лет до нескольких месяцев.</p>"
		}
	},

	{
		"layout": "text-half",
		"section": {
			"color": "light",
			"id": "signup",
			"title": "<mark>Записывайся</mark> прямо сейчас!",
			"text": "...и приходи на мастер-класс 26 июня в 15:00 по Москве"
		},
		"content": {
			"swap": false,
			"media": {
				"type": "form",
				"form": {
					"type": "messengers",
					"carrotquest_event": "Лид: «МК-АВТО / Веб-дизайн»",
					"orientation": "v",
					"title": "Записаться на&nbsp;мастер-класс",
					"subtitle": "Переходи в любимый мессенджер, нажимай «Старт» и&nbsp;получай инструкции в&nbsp;течение пяти минут. Всё быстро, просто и понятно!",
					"messengers": [
						{
							// "social": "telegram",
							"link": "https://t.me/va_school_bot?start=ml6",
							"text": "Перейти в Telegram"
						}
					]
				}
			},
			"text": "<p>Оставляй свои данные и переходи в Telegram (ссылка будет после отправки формы). Все анонсы, обновления и&nbsp;материалы мы&nbsp;будем публиковать там.</p><p>Что необходимо для участия:</p><ol><li><b>Время.</b> Выдели полтора часа в&nbsp;указанный день, чтобы внимательно посмотреть мастер-класс.</li><li><b>Хорошее настроение.</b> Грустных людей не&nbsp;стрим не&nbsp;пускаем. У&nbsp;нас всегда весело и&nbsp;интересно, этого&nbsp;же ждём и&nbsp;от&nbsp;тебя!</li></ol>"
		}
	}
]
