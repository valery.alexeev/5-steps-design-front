export default [

	{
		"layout": "video",
		"section": {
			"color": "white"
		},
		"content": {
			"youtube_id": "VDg361culKA",
			"src": {
				"mp4": "https://va-promotion.ru/video/webdesign-mc/video.mp4"
			},
			"title": "Мастер-класс по веб-дизайну",
			"text": "<p></a>",
			"links": [
				{
					"title": "Figma",
					"text": "Графический редактор для дизайна",
					"url": "https://figma.com"
				},
				{
					"title": "Miro",
					"text": "Интерактивная онлайн-доска",
					"url": "https://miro.com"
				},
				{
					"title": "Freepik",
					"text": "Бесплатная стоковая графика",
					"url": "https://freepik.com"
				},
				{
					"title": "Flaticon",
					"text": "Бесплатные иконки для сайта",
					"url": "https://flaticon.com"
				}
			],

		}
	}
]
