export default [

	{
		"layout": "video",
		"section": {
			"color": "white"
		},
		"content": {
			"src": {
				"mp4": "https://va-promotion.ru/video/webdesign-mc/video.mp4",
				"webm": "https://va-promotion.ru/video/webdesign-mc/video.webm"
			},
			"title": "Мастер-класс по веб-дизайну",
			"links": [
				{
					"title": "Figma",
					"text": "Графический редактор для дизайна",
					"url": "https://figma.com"
				},
				{
					"title": "Miro",
					"text": "Интерактивная онлайн-доска",
					"url": "https://miro.com"
				},
				{
					"title": "Freepik",
					"text": "Бесплатная стоковая графика",
					"url": "https://freepik.com"
				},
				{
					"title": "Flaticon",
					"text": "Бесплатные иконки для сайта",
					"url": "https://flaticon.com"
				}
			],




			"autowebinar": {
				"schedule": "daily",
				"startsAt": {
					"h": 21,
					"m": 0,
					"s": 0
				},
				"length": {
					"h": 1,
					"m": 10,
					"s": 56
				},
				"resetAfter": {
					"h": 22
				},
				"tz": "Asia/Tbilisi",

				"autocomments": {
					"00:01:30": {
						"name": "Инна",
						"text": "Всем привет!))"
					},
					"00:01:36": {
						"name": "Виктор Астафьев",
						"text": "Добрый вечер"
					},


					"00:02:39": {
						"name": "Виктор Астафьев",
						"text": "+++"
					},
					"00:02:43": {
						"name": "Anna",
						"text": "Всё слышно"
					},
					"00:02:47": {
						"name": "Вадим",
						"text": "Хорошо слышно и видно"
					},
					"00:02:44": {
						"name": "Инна",
						"text": "У меня немного лагает("
					},
					"00:02:51": {
						"name": "Yulia Pimenova",
						"text": "Слышно, видно"
					},
					"00:03:03": {
						"name": "Дарья Сергеева",
						"text": "Слышно и видно) всё окей)"
					},
					"00:03:11": {
						"name": "Инна",
						"text": "Вроде бы всё ок сейчас)"
					},
					"00:04:03": {
						"name": "Виталий Пивиков",
						"text": "Всё отлично, видно и слышно"
					},



					"00:04:31": {
						"name": "Инна",
						"text": "Екатеринбург, 27 лет"
					},
					"00:04:34": {
						"name": "Виталий Пивиков",
						"text": "Санкт-Петербург"
					},
					"00:04:35": {
						"name": "Yulia Pimenova",
						"text": "Ярославль"
					},
					"00:04:39": {
						"name": "Дарья Сергеева",
						"text": "Я из Перьми)"
					},
					"00:04:40": {
						"name": "Вадим",
						"text": "Тоже Ярославль ✌🏻"
					},


					"00:05:54": {
						"name": "Виктор Астафьев",
						"text": "Круто! А как начали работать со штатами?"
					},
					"00:06:05": {
						"name": "Дарья Сергеева",
						"text": "++++"
					},
					"00:06:06": {
						"name": "Дарья Сергеева",
						"text": "Да, тоже было бы интересно узнать об этом!)"
					},


					"00:07:56": {
						"name": "Инна",
						"text": "Разработка сайтов) правильное расположение элментов и выбор цветов"
					},
					"00:07:57": {
						"name": "Виктор Астафьев",
						"text": "Веб-дизайн — это выстраивание композиции, компоновка и программирование) правильно?"
					},
					"00:08:09": {
						"name": "Дарья Сергеева",
						"text": "Веб дизайн это творчество, создание красивого, приятного дизайна страниц сайта."
					},
					"00:08:12": {
						"name": "Вадим",
						"text": "Что бы было понятно куда нажимать \nИ чтобы это было красиво"
					},


					"00:11:26": {
						"name": "Дарья Сергеева",
						"text": "Художник самоучка, студент😁"
					},
					"00:11:29": {
						"name": "Виктор Астафьев",
						"text": "Художник, немного разбирался в программировании"
					},
					"00:11:30": {
						"name": "Инна",
						"text": "Архитектор, учусь на 3м курсе)"
					},
					"00:11:34": {
						"name": "Вадим",
						"text": "Учусь на программиста, но понял что мне ближе дизайн)"
					},
					"00:11:37": {
						"name": "Yulia Pimenova",
						"text": "Графический дизайн, чуть веб есть пробелы)"
					},
					"00:11:38": {
						"name": "Виталий Пивиков",
						"text": "Филолог, переводчик"
					},
					"00:11:52": {
						"name": "Anna",
						"text": "Я профессиональный художник,работаю в основном в реставрации или оформлении городских выставок)"
					},



					"00:15:50": {
						"name": "Yulia Pimenova",
						"text": "Вау, спасибо) круто!"
					},
					"00:15:56": {
						"name": "Инна",
						"text": "Офигенный генератор 🔥"
					},
					"00:16:03": {
						"name": "Виталий Пивиков",
						"text": "А можно ли будет использовать генератор после мастер-класса?)"
					},
					"00:16:21": {
						"name": "Вадим",
						"text": "А где найти этот генератор? Я не догнал чего-то 🤣"
					},
					"00:16:34": {
						"name": "Инна",
						"text": "Вадим, будет ссылка после окончания МК"
					},



					"00:22:13": {
						"name": "Виталий Пивиков",
						"text": "А запись мастер-класса будет доступна?"
					},



					"01:09:46": {
						"name": "Yulia Pimenova",
						"text": "Спасибо, классный мастер-класс😊"
					},
					"01:09:49": {
						"name": "Вадим",
						"text": "Все круто! спасибо"
					},
					"01:09:51": {
						"name": "Инна",
						"text": "Спасибо большое Вам🌸"
					}
				},

				"comments_allowed": true
			}

		}
	}
]
