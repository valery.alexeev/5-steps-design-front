export default [
	{
		"layout": "intro",
		"section": {
			"color": "white",
			"id": "intro"
		},
		"content": {
			"pre": [
				{
					"style": "bold",
					"text": "Бесплатно"
				},
				{
					"style": "normal",
					"text": "Экспресс-курс за 8 дней"
				}
			],
			"title": "Пять шагов для <mark>быстрого старта</mark> в&nbsp;веб‑дизайне",
			"text": "Бесплатный экспресс-курс для изучения основ веб-дизайна с&nbsp;нуля. Восемь дней, восемь шагов, более 100 полезных статей, лекций и&nbsp;инструментов. Идеальная пошаговая структура и&nbsp;никакой &laquo;воды&raquo;&nbsp;&mdash; только реально полезная информация.",
			"image": {
				"src": "/index/intro.png",
				"width": 740
			},
			"button": {
				"text": "Начать изучение курса",
				"emoji": "🤟🏻",
				"href": "#signup"
			}
		}
	},

	{
		"layout": "features",
		"section": {
			"color": "light",
			"title": "Что тебя <mark>ждёт на курсе?</mark>",
			"text": "Схема обучения на&nbsp;этом курсе проверена годами. Пять простых и&nbsp;понятных шагов: от&nbsp;знакомства с&nbsp;дисциплиной до&nbsp;практики и&nbsp;прокачки навыков."
		},
		"content": {
			"columns": 3,
			"image_height": "100px",
			"cards": [
				{
					"emoji": "🖖🏻",
					"title": "Знакомство с веб-дизайном",
					"text": "Для начала мы&nbsp;познакомимся с&nbsp;профессией веб-дизайнера и&nbsp;её&nbsp;особенностями. Мы&nbsp;также сравним и&nbsp;выберем для тебя идеальный графический редактор."
				},
				{
					"emoji": "📚",
					"title": "Изучение теории",
					"text": "Мы&nbsp;последовательно изучим композицию, типографику, цветовую теорию и&nbsp;интерфейсы&nbsp;&mdash; четыре важнейших составляющих крутого дизайна."
				},
				{
					"emoji": "🖍",
					"title": "Практика в дизайне",
					"text": "Мы&nbsp;решим несколько интересных дизайнерских задачек, набьём руку и&nbsp;научимся наполнять портфолио без реальных заказов."
				},
				{
					"emoji": "👀",
					"title": "Прокачка насмотренности",
					"text": "Мы&nbsp;научимся развивать чувство стиля, найдём идеальные источники вдохновения и&nbsp;познакомимся с&nbsp;лучшими дизайн-студиями, за&nbsp;которыми стоит следить."
				},
				{
					"emoji": "🤓",
					"title": "Развитие soft-скиллов",
					"text": "Мы&nbsp;научимся общаться с&nbsp;заказчиками и&nbsp;прокачаемся в&nbsp;менеджменте, деловом общении, маркетинге и&nbsp;других навыках обязательных для дизайнера."
				}
			]
		}
	},

	{
		"layout": "text-half",
		"section": {
			"id": "format",
			"color": "white",
			"title": "В <mark>удобном для тебя</mark> формате",
			"text": "Мы&nbsp;подобрали материалы для изучения так, чтобы тебе не&nbsp;было скучно: статьи идут вперемешку с&nbsp;видео-лекциями и&nbsp;практикой."
		},
		"content": {
			"swap": true,
			"align": "start",
			"media": {
				"type": "image",
				"image": "/index/types@2x.png"
			},
			"text": "<p>Каждый из&nbsp;шагов в&nbsp;данном курсе содержит обилие материалов в&nbsp;самых <strong>разных форматах</strong>. Здесь есть и&nbsp;длинные статьи-руководства, и&nbsp;полезные видео-ролики с&nbsp;YouTube, и&nbsp;авторские материалы, и&nbsp;даже интерактивные тренажёры для дизайнеров. Благодаря такому подходу, каждый участник курса сможет найти для себя идеальный формат.</p><p>Мы&nbsp;также добавили несколько наиболее <strong>важных видео-лекций</strong> из&nbsp;нашего закрытого курса. Мы&nbsp;считаем, что информация о&nbsp;самых значимых правилах и&nbsp;принципах веб-дизайна должна быть доступно каждому, кто решил связать свою жизнь с&nbsp;данной профессией.</p>"
		}
	},

	{
		"layout": "text-half",
		"section": {
			"color": "light",
			"id": "signup",
			"title": "<mark>Начни курс</mark> прямо сейчас!",
			"text": "Без оплаты, рассрочек и&nbsp;дополнительных условий под звёздочкой. Получи восемь шагов (по&nbsp;одному в&nbsp;день) с&nbsp;самыми важными материалами для изучения веб-дизайна."
		},
		"content": {
			"swap": false,
			"media": {
				"type": "form",
				"oldform": {
					"type": "default",
					"sendpulse_list": 5933,
					"carrotquest_event": "Лид: «Подписка на 5 шагов»",
					"orientation": "v",
					"title": "Записаться на&nbsp;участие в&nbsp;курсе",
					"subtitle": "Оставьте свой Email и&nbsp;получите первую лекцию курса в&nbsp;течение пяти минут. Никакого спама в&nbsp;вашем ящике!",
					"redirect": "/success/",
					"button": "Записаться на курс"
				},

				"form": {
					"type": "messengers",
					"carrotquest_event": "Лид: «Подписка на 5 шагов»",
					"orientation": "v",
					"title": "Записаться на&nbsp;участие в&nbsp;курсе",
					"subtitle": "Переходи в любимый мессенджер, нажимай «Старт» и&nbsp;получай первую лекцию курса в&nbsp;течение пяти минут. Всё быстро, просто и понятно!",
					"messengers": [
						{
							"social": "telegram",
							"link": "https://t.me/va_school_bot?start=ml8",
							"text": "Начать курс в Telegram"
						}
					]
				}
			},
			"text": "<p>Твоя программа обучения на&nbsp;курсе будет выглядеть так:</p><ol><li><strong>Знакомство с&nbsp;веб-дизайном.</strong> О&nbsp;профессии и&nbsp;приложениях.</li><li><strong>Теория веб-дизайна:</strong><ol><li><strong>Композиция.</strong> Правила расположения блоков.</li><li><strong>Типографика.</strong> Учимся работать с&nbsp;текстом и шрифтами.</li><li><strong>Цвет.</strong> Цветовая теория, оттенки, палитры и ассоциации.</li><li><strong>Интерфейсы.</strong> Работаем с&nbsp;кнопками, полями и&nbsp;прочими элементами.</li></ol></li><li><strong>Практика в&nbsp;дизайне.</strong> Работа с&nbsp;портфолио и&nbsp;дизайнерскими задачами.</li><li><strong>Насмотренность.</strong> Развиваем вкус и&nbsp;чувство стиля.</li><li><strong>Soft-навыки.</strong> Прокачиваемся в&nbsp;смежных навыках.</li></ol>"
		}
	}
]
