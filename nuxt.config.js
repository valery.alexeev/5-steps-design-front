export default {
	// Global page headers (https://go.nuxtjs.dev/config-head)
	head: {
		title: 'Пять шагов в веб-дизайне',
		meta: [
			{
				charset: 'utf-8'
			},
			{
				name: 'viewport',
				content: 'width=device-width, initial-scale=1'
			},
			{
				hid: 'description',
				name: 'description',
				content: ''
			},
			{
				name: 'msapplication-TileColor',
				hid: 'msapplication-TileColor',
				content: '#82b822'
			},
			{
				name: 'theme-color',
				hid: 'theme-colorr',
				content: '#ffffff'
			}
		],
		link: [
			{
				rel: "preconnect",
				href: "https://fonts.gstatic.com"
			},
			{
				href: "https://fonts.googleapis.com/css2?family=Inter:wght@400;700;800;900&display=swap",
				rel: "stylesheet"
			},

			{
				rel: "apple-touch-icon",
				sizes: "180x180",
				href: "/favicon/apple-touch-icon.png"
			},
			{
				rel: "icon",
				sizes: "32x32",
				href: "/favicon/favicon-32x32.png",
				type: "image/png"
			},
			{
				rel: "icon",
				sizes: "16x16",
				href: "/favicon/favicon-16x16.png",
				type: "image/png"
			},
			{
				rel: "manifest",
				href: "/favicon/site.webmanifest",
			}
		],
	},

	// Global CSS (https://go.nuxtjs.dev/config-css)
	css: [],

	// Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
	plugins: [
		{src: '~/plugins/chat-scroll.js', ssr: false}
	],

	// Auto import components (https://go.nuxtjs.dev/config-components)
	components: {
		dirs: [
			'~/components',
			'~/components/ui',
			'~/components/blocks',
		]
	},

	// Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
	buildModules: [],

	// Modules (https://go.nuxtjs.dev/config-modules)
	modules: [
		'@nuxtjs/style-resources',
		'@nuxtjs/axios'
	],

	axios: {
	},

	styleResources: {
		scss: [
			'~/assets/scss/_variables.scss',
			'~/assets/scss/_mixins.scss',
		]
	},

	// Build Configuration (https://go.nuxtjs.dev/config-build)
	build: {
		extend (config) {
			config.resolve.alias['~blocks'] = '~/components/builder'
		}
	},

	server: {
        host: "0.0.0.0"
    }
}
